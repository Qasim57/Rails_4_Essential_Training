class SubjectsController < ApplicationController

  layout "admin"

  before_action :confirm_logged_in

  def index
  	@subjects = Subject.order("position ASC")
  end

  def show
    @subject = Subject.find(params[:id])
  end

  def new
    @subject = Subject.new({:name => "Default"})
    @subject_count = Subject.count + 1
  end

  def create
    #Instantiate a new object using form parameters
    @subject = Subject.new(subject_params)
    #Save the object
    if @subject.save
      #If saving succeeds, we redirect to the index action
      flash[:notice] = "Subject created successfully"
      redirect_to(:action => 'index')
    else
    #Saving falsed, redisplay the form
      @subject_count = Subject.count + 1
      render('new')
    end
  end

  def edit
    @subject = Subject.find(params[:id])
    @subject_count = Subject.count
  end

  def update
    #Find an existing object using form parameters
    @subject = Subject.find(params[:id])

    #Update the object
    if @subject.update_attributes(subject_params)
      #If saving succeeds, we redirect to the index action
      flash[:notice] = "Subject updated successfully"
      redirect_to(:action => 'show', :id => @subject.id)
    else
    #Saving falsed, redisplay the form
      @subject_count = Subject.count
      render('edit')
    end
  end  

  def delete
    @subject = Subject.find(params[:id])
  end

  def destroy
    subject = Subject.find(params[:id]).destroy
    flash[:notice] = "Subjecto '#{subject.name}' deleted successfully"
    redirect_to(:action => 'index')
  end

  private

    def subject_params
      # same as using "params[:subject]", except that it:
      # - raises an error if :subject is not present
      # - allows listed attributes to be mass-assigned
      params.require(:subject).permit(:name, :position, :visible, :created_at)
    end

end