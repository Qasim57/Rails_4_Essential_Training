class DemoController < ApplicationController
	
	layout 'application'
  	
  	def index
  		# render("hello")
  	end

  	def hello
  		#render("index")
  		@array = [1,2,2,4,5]
  		@id = params['id'].to_i
  		@page = params[:page].to_i
  	end

  	def other_hello
  		redirect_to("http://boogle.com")
  		#redirect_to(:controller => 'demo', :action => 'hello')
  	end


    def text_helpers
    end

    def escape_output
    end

    def make_error
      #render(:text => "test" #Syntax error
      #render(:text => @something.upcase) #undefined method
      render(:text => "1" + 1) #Can't convert type
    end

    def logging
      logger.debug("This is debug.")
      logger.info("This is info.")
      logger.warn("This is a warning.")
      logger.error("This is an error.")
      logger.fatal("This is fatal.")
      render(:text => 'Logged!')
    end
end