class SectionsController < ApplicationController

  layout "admin"

  before_action :confirm_logged_in
  before_action :find_page
  
  def index
    #@sections = Section.all
    #@section = Section.where(:page_id => @page.id).sorted
    @sections = @page.sections.sorted

  end

  def show
    @section = Section.find(params[:id])
  end

  def new
    @page_count = Page.count + 1
    @section = Section.new({:page_id => @page.id})
  end

  def create
    @section = Section.new(section_params)
    if @section.save
      redirect_to(:action => 'show', :id => @section.id, :page_id => @page.id)
    else
      @page_count = Page.count
      render(edit)
    end
  end

  def edit
    @page_count = Page.count
    @section = Section.find(params[:id])
  end

  def update
    @section = Section.find(params[:id])
    if @section.update_attributes(section_params)
      flash[:notice] = "Section has been updated"
      redirect_to(:action => 'show', :id => @section.id, :page_id => @page.id)
    else
      @page_count = Page.count
      render(edit)
    end
  end  

  def delete
    @section = Section.find(params[:id])
  end

  def destroy
    section = Section.find(params[:id]).destroy
    flash[:notice] = "Section '#{section.name}' has been removed"
    redirect_to(:action => 'index', :page_id => @page.id)
  end
  
  private
    def section_params
      params.require(:section).permit(:id, :page_id, :name, :position, :visible, :content_type, :content, :created_at, :updated_at)
    end

    def find_page
      if params[:page_id]
        @page = Page.find(params[:page_id])
      end
    end

end
