class AlterUsers < ActiveRecord::Migration

  def up
  	rename_table("users", "admin_users")#this is reversible, if called in a def change method
  	add_column("admin_users", "username", :string, :limit => 25, :after => "email")#:after is not supported across all databases, works for mysql 
  	change_column("admin_users", "email", :string, :limit => 100)
  	rename_column("admin_users", "password", "hashed_password")
  	puts("*** About to add an index here ***")
  	add_index("admin_users", "username")
  end

  def down
  	remove_index("admin_users", "username")
  	rename_column("admin_users", "hashed_password", "password")
  	change_column("admin_users", "email", :string, :default => "", :null => false)
  	remove_column("admin_users", "username")
  	rename_table("admin_users", "users")
  end

end
